Main = {
    delete: function (el, url_d, token) {
        if (url_d && token) {
            if (confirm('Удалить элемент')) {
                const data = {
                    _method: 'DELETE',
                    _token: token,
                };
                const success = function (data) {
                    if (data.status === 'success') {
                        el.closest('tr').remove();
                    }
                };
                this.sendRequest('POST', url_d, data, success);
            }
        }
        return true;
    },
    sendRequest: function(type, url, data, success, error = null) {
        $.ajax({
            type: type,
            url: url,
            cache: false,
            dataType: 'json',
            data: data,
            success: success,
            error: error
        });
    }
}
