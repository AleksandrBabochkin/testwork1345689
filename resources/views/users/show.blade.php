@extends('master')
@section('content')
    <div class="container">
        <div class="form-group">
            <label for="exampleInputName">{{ __('user.name') }}</label>
            <input type="text" name="name" class="form-control" id="exampleInputName" readonly value="{{ $user->name }}">
        </div>
        <div class="form-group">
            <label for="exampleInputEmail">{{ __('user.email') }}</label>
            <input type="email" name="email" class="form-control" id="exampleInputEmail" readonly value="{{ $user->email }}">
        </div>
        <a type="button" class="btn btn-primary" href="{{ route('users.index') }}">{{ __('html.back') }}</a>
    </div>
@endsection
