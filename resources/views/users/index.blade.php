@extends('master')
@section('content')
    <div class="container">
        <div class="jumbotron">
            <table class="table">
                <thead>
                <tr>
                    <th scope="col">{{ __('user.index.table.id') }}</th>
                    <th scope="col">{{ __('user.index.table.name') }}</th>
                    <th scope="col">{{ __('user.index.table.email') }}</th>
                    <th scope="col">{{ __('html.action') }}</th>
                </tr>
                </thead>
                <tbody>
                @foreach($users as $key => $user)
                    <tr data-row-id="{{ $user->id }}">
                        <th scope="row">{{ $user->id }}</th>
                        <td> <a href="{{ route('users.edit', $user->id) }}">{{ $user->name }}</a></td>
                        <td>{{ $user->email }}</td>
                        <td>
                            <button onclick="Main.delete(this, '{{ route('users.destroy', $user->id) }}', '{{ csrf_token() }}')"
                                    type="button" class="btn btn-danger">{{ __('html.delete') }}
                            </button>
                            <a href="{{ route('users.show', $user->id) }}" type="button" class="btn btn-info">
                                {{ __('html.show') }}
                            </a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection
