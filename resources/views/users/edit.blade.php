@extends('master')
@section('content')
    <div class="container">
        <form action="{{ route('users.update', $user->id) }}" method="POST">
            {{ csrf_field() }}
            {{ method_field('PATCH') }}
            <div class="form-group">
                <label for="exampleInputName">{{ __('user.name') }}</label>
                <input type="text" name="name" class="form-control" id="exampleInputName" required
                       placeholder="{{ __('user.create.enter_name') }}" value="{{ $user->name }}">
            </div>
            <div class="form-group">
                <label for="exampleInputEmail">{{ __('user.email') }}</label>
                <input type="email" name="email" class="form-control" id="exampleInputEmail" required
                       placeholder="{{ __('user.create.enter_email') }}" value="{{ $user->email }}">
            </div>
            <div class="form-group">
                <label for="exampleInputPassword">{{ __('user.password') }}</label>
                <input type="password" name="password" class="form-control" id="exampleInputPassword"
                       placeholder="{{ __('user.create.enter_password') }}">
            </div>
            <button type="submit" class="btn btn-primary">{{ __('html.save') }}</button>
        </form>
    </div>
@endsection
