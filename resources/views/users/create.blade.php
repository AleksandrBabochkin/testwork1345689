@extends('master')
@section('content')
    <div class="container">
        <form action="{{ route('users.store') }}" method="POST">
            {{ csrf_field() }}
            <div class="form-group">
                <label for="exampleInputName">{{ __('user.name') }}</label>
                <input type="text" name="name" class="form-control" id="exampleInputName" required
                       placeholder="{{ __('user.create.enter_name') }}">
            </div>
            <div class="form-group">
                <label for="exampleInputEmail">{{ __('user.email') }}</label>
                <input type="email" name="email" class="form-control" id="exampleInputEmail" required
                       placeholder="{{ __('user.create.enter_email') }}">
            </div>
            <div class="form-group">
                <label for="exampleInputPassword">{{ __('user.password') }}</label>
                <input type="password" name="password" class="form-control" id="exampleInputPassword" required
                       placeholder="{{ __('user.create.enter_password') }}">
            </div>
            <button type="submit" class="btn btn-primary">{{ __('html.save') }}</button>
        </form>
    </div>
@endsection
