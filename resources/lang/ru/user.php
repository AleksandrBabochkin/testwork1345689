<?php
return [
    'name' => 'Имя',
    'email' => 'Email',
    'password' => 'Пароль',

    'index' => [
        'title' => 'Все пользователи',
        'table' => [
            'id' => 'ID',
            'name' => 'Имя пользователя',
            'email' => 'Email'
        ],
    ],

    'create' => [
        'title' => 'Создание пользователя',
        'enter_name' => 'Введите имя',
        'enter_email' => 'Введите email',
        'enter_password' => 'Введите пароль',
    ],

    'show' => [
        'title' => 'Просмотр пользователя',
    ],

    'edit' => [
        'title' => 'Редактирование пользователя',
    ],
];
