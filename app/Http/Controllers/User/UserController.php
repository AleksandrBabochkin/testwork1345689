<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Http\Requests\Users\{UserCreateRequest, UserUpdateRequest};
use App\Models\User;

class UserController extends Controller
{
    public const TEMPLATE_PATH = 'users.';

    public function index()
    {
        $with = [
            'users' => User::get(),
            'title' => __('user.index.title'),
        ];
        return view(self::TEMPLATE_PATH . __FUNCTION__)->with($with);
    }

    public function create()
    {
        $with = [
            'title' => __('user.create.title'),
        ];
        return view(self::TEMPLATE_PATH . __FUNCTION__)->with($with);
    }

    public function store(UserCreateRequest $request)
    {
        User::create($request->toArray());

        return redirect(route('users.index'));
    }

    public function show(User $user)
    {
        $with = [
            'user' => $user,
            'title' => __('user.show.title'),
        ];
        return view(self::TEMPLATE_PATH . __FUNCTION__)->with($with);
    }

    public function edit(User $user)
    {
        $with = [
            'user' => $user,
            'title' => __('user.edit.title'),
        ];
        return view(self::TEMPLATE_PATH . __FUNCTION__)->with($with);
    }

    public function update(UserUpdateRequest $request, User $user)
    {
        $user->update($request->toArray());

        return redirect(route('users.index'));
    }

    public function destroy($id)
    {
        $user = User::FindOrFail($id);
        $user->delete();

        return response()->json(['status' => 'success']);
    }
}
